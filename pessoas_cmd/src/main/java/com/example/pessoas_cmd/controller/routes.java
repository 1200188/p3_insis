package com.example.pessoas_cmd.controller;


import com.example.dto.PessoaSchema;
import com.example.pessoas_cmd.config.Messaging;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RequestMapping("pessoa")
@RestController
public class routes {


    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public ResponseEntity<String> createPessoa(@RequestBody PessoaSchema pessoa) throws ParseException {


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProva = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + pessoa.getName() , HttpMethod.GET, entity,String.class);
        if (responseProva.getStatusCodeValue()==200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Pessoa already exists \"}");
        }


        template.convertAndSend(Messaging.EXCHANGE_PESSOA, Messaging.ROUTING_KEY_CREATE, pessoa);
        return ResponseEntity.accepted().body("{\"message\": \" The new 'Pessoa' is accepted \"}");
    }

    @PatchMapping("/pessoaName/{name}/role/{role}")
    public ResponseEntity<String> setRole(@PathVariable ("name") String nomePessoa, @PathVariable ("role") String role) throws IOException, ParseException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + nomePessoa , HttpMethod.GET, entity,String.class);
        if (responsePessoa.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Pessoa do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responsePessoa.getBody());
        PessoaSchema pessoa = mapper.readValue(reader, PessoaSchema.class);

        if (pessoa.getRole()!=null) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Role attribution already done \"}");
        }

        switch (role) {
            case "student":
                break;
            case "teacher":
                break;
            case "regent":
                break;
            default:
                return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Role not valid \"}");
        }



        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();


        pessoa.setStatus("active");
        pessoa.setRole(role);
        pessoa.setLastModification(now.format(format).toString());


        template.convertAndSend(Messaging.EXCHANGE_PESSOA, Messaging.ROUTING_KEY_UPDATE_PESSOA, pessoa);
        return ResponseEntity.accepted().body("{\"message\": \" The role of pessoa "+ pessoa.getName() +" will be set \"}");
    }

    @PatchMapping("/pessoaName/{name}/activate")
    public ResponseEntity<String> setStatusActive(@PathVariable ("name") String nomePessoa) throws IOException, ParseException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + nomePessoa , HttpMethod.GET, entity,String.class);
        if (responsePessoa.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Pessoa do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responsePessoa.getBody());
        PessoaSchema pessoa = mapper.readValue(reader, PessoaSchema.class);

        if (pessoa.getRole()!="student") {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Only students may be activated \"}");
        } else if (pessoa.getStatus()!="active") {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Status of student is already active \"}");
        }

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();


        pessoa.setStatus("active");
        pessoa.setLastModification(now.format(format).toString());


        template.convertAndSend(Messaging.EXCHANGE_PESSOA, Messaging.ROUTING_KEY_UPDATE_PESSOA, pessoa);
        return ResponseEntity.accepted().body("{\"message\": \" The role of pessoa "+ pessoa.getName() +" will be set \"}");
    }


    @DeleteMapping("/pessoaName/{name}")
    public ResponseEntity<String> deleteProva(@PathVariable ("name") String nomePessoa) throws IOException {


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + nomePessoa , HttpMethod.GET, entity,String.class);
        if (responsePessoa.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Pessoa do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responsePessoa.getBody());
        PessoaSchema pessoa = mapper.readValue(reader, PessoaSchema.class);

        template.convertAndSend(Messaging.EXCHANGE_PESSOA, Messaging.ROUTING_KEY_DELETE, pessoa);
        return ResponseEntity.accepted().body("{\"message\": \" Pessoa "+ pessoa.getName() +" will be deleted \"}");
    }

}
