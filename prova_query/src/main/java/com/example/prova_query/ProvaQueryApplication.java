package com.example.prova_query;

import com.example.prova_query.consumer.ProvaConsumer;
import com.example.prova_query.controller.Routes;
import com.example.prova_query.provaConfig.Messaging;
import com.example.prova_query.repo.ProvaRepo;
import com.example.prova_query.service.ProvaService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackageClasses = {ProvaConsumer.class, Messaging.class, ProvaService.class, ProvaRepo.class, Routes.class})
@EnableJpaRepositories("com.example.prova_query.repo")
public class ProvaQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaQueryApplication.class, args);
	}

}
