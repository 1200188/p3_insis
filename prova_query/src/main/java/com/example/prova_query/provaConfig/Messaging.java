package com.example.prova_query.provaConfig;

import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class Messaging {

    public static final String EXCHANGE = "prova";
    public static final String QUEUE_CREATE = "prova_recebida";
    public static final String QUEUE_UPDATE_SALA_RECEBIDA = "sala_recebida";
    public static final String QUEUE_UPDATE_PROVA_CONFIRMADA = "prova_confirmada";
    public static final String QUEUE_DELETE = "delete_prova";


    public static final String ROUTING_KEY_CREATE = "prova_recebida_routingKey";
    public static final String ROUTING_KEY_SALA_RECEBIDA = "sala_recebida_routingKey";
    public static final String ROUTING_KEY_PROVA_CONFIRMADA = "prova_confirmada_routingKey";
    public static final String ROUTING_KEY_DELETE = "delete_prova_routingKey";




    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    public Queue queueCreate() {
        return new Queue(QUEUE_CREATE);
    }

    @Bean
    public Binding bindingCreate(Queue queueCreate, TopicExchange exchange) {
        return BindingBuilder.bind(queueCreate).to(exchange).with(ROUTING_KEY_CREATE);
    }

    @Bean
    public Queue queueSalaProvaRecebida() {
        return new Queue(QUEUE_UPDATE_SALA_RECEBIDA);
    }

    @Bean
    public Binding bindingSalaProvaRevebida(Queue queueSalaProvaRecebida, TopicExchange exchange) {
        return BindingBuilder.bind(queueSalaProvaRecebida).to(exchange).with(ROUTING_KEY_SALA_RECEBIDA);
    }

    @Bean
    public Queue queueSalaProvaConfirmada() {
        return new Queue(QUEUE_UPDATE_PROVA_CONFIRMADA );
    }

    @Bean
    public Binding bindingProvaConfirmada(Queue queueSalaProvaConfirmada, TopicExchange exchange) {
        return BindingBuilder.bind(queueSalaProvaConfirmada).to(exchange).with(ROUTING_KEY_PROVA_CONFIRMADA);
    }

    @Bean
    public Queue queueDelete() {
        return new Queue(QUEUE_DELETE);
    }

    @Bean
    public Binding bindingDelete(Queue queueDelete, TopicExchange exchange) {
        return BindingBuilder.bind(queueDelete).to(exchange).with(ROUTING_KEY_DELETE);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}
