package com.example.proposta_cmd.controller;

import com.example.dto.PropostaSchema;
import com.example.proposta_cmd.config.Messaging;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RequestMapping("proposta")
@RestController
public class Routes {


    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public ResponseEntity<String> createProposta(@RequestBody PropostaSchema proposta) throws ParseException {


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProposta = restTemplate.exchange("http://localhost:9299/proposta/propostaName/" + proposta.getName() , HttpMethod.GET, entity,String.class);
        if (responseProposta.getStatusCodeValue()==200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Proposta already exists \"}");
        }

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + proposta.getProponent(), HttpMethod.GET, entity,String.class);
        if (responsePessoa.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Proponent do not exists \"}");
        }

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();

        proposta.setLastModification(now.format(format).toString());
        proposta.setStatus("new");
        template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_CREATE, proposta);
        return ResponseEntity.accepted().body("{\"message\": \" The new 'Proposta' is accepted \"}");
    }

    @PatchMapping("/propostaName/{name}/status/{status}")
    public ResponseEntity<String> setStatus(@PathVariable ("name") String nomeProposta, @PathVariable ("status") String status) throws IOException, ParseException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProposta = restTemplate.exchange("http://localhost:9299/proposta/propostaName/" + nomeProposta , HttpMethod.GET, entity,String.class);
        if (responseProposta.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Proposta do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responseProposta.getBody());
        PropostaSchema proposta = mapper.readValue(reader, PropostaSchema.class);

        if (!proposta.getStatus().equals("new")) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" RUC already set the status for this proposal or it expired \"}");
        }

        switch (status) {
            case "accepted":
                break;
            case "rejected":
                template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_PROPOSTA_REJEITADA, proposta);
                break;
            case "canceled":
                template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_PROPOSTA_REJEITADA, proposta);
                break;
            default:
                return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Status not valid \"}");
        }



        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();


        proposta.setLastModification(now.format(format).toString());
        proposta.setStatus(status);


        template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_UPDATE_PROPOSTA_STATUS, proposta);
        return ResponseEntity.accepted().body("{\"message\": \" The status of "+ proposta.getName() +" will be set \"}");
    }

    @PatchMapping("/propostaName/{name}/student/{student}")
    public ResponseEntity<String> setStudent(@PathVariable ("name") String nomeProposta, @PathVariable ("student") String student) throws IOException, ParseException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProposta = restTemplate.exchange("http://localhost:9299/proposta/propostaName/" + nomeProposta , HttpMethod.GET, entity,String.class);
        if (responseProposta.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Proposta do not exists \"}");
        }

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + student, HttpMethod.GET, entity,String.class);
        if (responsePessoa.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Student do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responseProposta.getBody());
        PropostaSchema proposta = mapper.readValue(reader, PropostaSchema.class);


        if (proposta.getStudent()!=null) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Student attribution already done \"}");
        }



        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();


        proposta.setLastModification(now.format(format).toString());
        proposta.setStudent(student);

        template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_UPDATE_PROPOSTA_STUDENT, proposta);
        return ResponseEntity.accepted().body("{\"message\": \" The student for proposta "+ proposta.getName() +" will be set \"}");
    }


    @DeleteMapping("/propostaName/{name}")
    public ResponseEntity<String> deleteProva(@PathVariable ("name") String nomeProposta) throws IOException {


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProposta = restTemplate.exchange("http://localhost:9299/proposta/propostaName/" + nomeProposta , HttpMethod.GET, entity,String.class);
        if (responseProposta.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Pessoa do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responseProposta.getBody());
        PropostaSchema proposta = mapper.readValue(reader, PropostaSchema.class);

        template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_DELETE, proposta);
        return ResponseEntity.accepted().body("{\"message\": \" Proposta "+ proposta.getName() +" will be deleted \"}");
    }

}
