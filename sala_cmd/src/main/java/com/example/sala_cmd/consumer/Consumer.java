package com.example.sala_cmd.consumer;

import com.example.dto.ProvaSchema;
import com.example.dto.ReservaSchema;
import com.example.dto.SalaSchema;
import com.example.sala_cmd.salaConfig.Messaging;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

@Component
public class Consumer {

    @Autowired
    private RabbitTemplate template;

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_SALA_RECEBIDA)
    public void consumeMessageFromQueueUpdate(ProvaSchema prova) throws ParseException, JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        //Validate date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime limiteMax = LocalDateTime.now().plusDays(7);
        Date dataLimiteMax = formatter.parse(limiteMax.format(format));
        LocalDateTime limiteMin = LocalDateTime.now().plusDays(1);
        Date dataLimiteMin = formatter.parse(limiteMin.format(format));
        Date dataProva = formatter.parse(prova.getDate());


        if (!(dataProva.compareTo(dataLimiteMax) < 0 && dataProva.compareTo(dataLimiteMin) > 0)) {
            template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_REJEITADA, prova);
            return;
        }

        //Validate Sala existence
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseSala = restTemplate.exchange("http://localhost:9295/sala/salaName/" + prova.getRoom(), HttpMethod.GET, entity, String.class);
        if (responseSala.getStatusCodeValue() != 200) {
            template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_REJEITADA, prova);
            return;
        }
        SalaSchema sala = mapper.readValue(responseSala.getBody(), SalaSchema.class);

        //Validate Sala availability
        ResponseEntity<String> responseReserva = restTemplate.exchange("http://localhost:9295/sala/reservas/", HttpMethod.GET, entity, String.class);
        ReservaSchema[] reservas = mapper.readValue(responseReserva.getBody(), ReservaSchema[].class);

        Calendar calendarProva = Calendar.getInstance();
        calendarProva.setTime(dataProva);
        int newProvaStart = calendarProva.get(Calendar.HOUR_OF_DAY);
        int newProvaEnd = calendarProva.get(Calendar.HOUR_OF_DAY) + 2;

        for (ReservaSchema reserva : reservas) {


            Date dataReserva = formatter.parse(reserva.getDate());
            Calendar calendarReserva = Calendar.getInstance();
            calendarReserva.setTime(dataReserva);

            int existingReservationStart = calendarReserva.get(Calendar.HOUR_OF_DAY);
            int existingReservationEnd;

            if (reserva.getEvent()) {
                existingReservationEnd = calendarReserva.get(Calendar.HOUR_OF_DAY) + reserva.getDuration();
            } else {
                existingReservationEnd = calendarReserva.get(Calendar.HOUR_OF_DAY) + 2;
            }


            if (calendarProva.get(Calendar.YEAR) == calendarReserva.get(Calendar.YEAR) && calendarProva.get(Calendar.DAY_OF_YEAR) == calendarReserva.get(Calendar.DAY_OF_YEAR)) {
                if (newProvaStart >= existingReservationStart && newProvaStart <= existingReservationEnd) {
                    template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_REJEITADA, prova);
                    return;
                } else if (existingReservationStart >= newProvaStart && existingReservationStart <= newProvaEnd) {
                    template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_REJEITADA, prova);
                    return;
                }
            }

        }
        ReservaSchema novaReserva = new ReservaSchema();
        novaReserva.setDate(prova.getDate());
        novaReserva.setEvent(false);
        novaReserva.setProva(prova.getName());
        novaReserva.setSala(sala.getName());
        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_CONFIRMADA, prova);
        template.convertAndSend(Messaging.EXCHANGE_SALA, Messaging.ROUTING_KEY_RESERVA, novaReserva);
    }


}
