package com.example.prova_query.consumer;

import com.example.dto.ProvaSchema;
import com.example.prova_query.enteties.ProvaEntity;
import com.example.prova_query.provaConfig.Messaging;
import com.example.prova_query.service.ProvaService;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProvaConsumer {

    @Autowired
    private ProvaService service;

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private ModelMapper modelMapper;


    @RabbitListener(queues = Messaging.QUEUE_CREATE)
    public void consumeMessageFromQueueCreate(ProvaSchema prova) {
        System.out.println("Message recieved from queue : " + prova);
        ProvaEntity provaConverted = modelMapper.map(prova, ProvaEntity.class);
        service.save(provaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_SALA_RECEBIDA)
    public void consumeMessageFromQueueUpdate(ProvaSchema prova) {
        ProvaEntity provaConverted = modelMapper.map(prova, ProvaEntity.class);
        service.save(provaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_DELETE)

    public void consumeMessageFromQueueDelete(ProvaSchema prova) {
        service.delete(prova.getName());
    }

}
