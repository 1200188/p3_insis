package com.example.proposta_query.config;

import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class Messaging {

    ///Exchange Pessoa
    public static final String EXCHANGE_PROPOSTA = "proposta";
    public static final String QUEUE_CREATE = "proposta_recebida";
    public static final String QUEUE_UPDATE_PROPOSTA_STATUS = "status_proposta_recebida";
    public static final String QUEUE_UPDATE_PROPOSTA_STUDENT = "student_proposta_recebida";
    public static final String QUEUE_DELETE = "delete_proposta";


    public static final String ROUTING_KEY_CREATE = "proposta_recebida_routingKey";
    public static final String ROUTING_KEY_UPDATE_PROPOSTA_STATUS = "status_proposta_recebida_routingKey";
    public static final String ROUTING_KEY_UPDATE_PROPOSTA_STUDENT = "student_proposta_recebida_routingKey";
    public static final String ROUTING_KEY_DELETE = "delete_proposta_routingKey";

    public static final String QUEUE_PROPOSTA_REJEITADA = "proposta_rejeitada";
    public static final String ROUTING_KEY_PROPOSTA_REJEITADA= "proposta_rejeitada_routingKey";




    ///Beans 4 Pessoa
    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_PROPOSTA);
    }

    @Bean
    public Queue queueCreate() {
        return new Queue(QUEUE_CREATE);
    }

    @Bean
    public Binding bindingCreate(Queue queueCreate, TopicExchange exchange) {
        return BindingBuilder.bind(queueCreate).to(exchange).with(ROUTING_KEY_CREATE);
    }

    @Bean
    public Queue queueCancel() {
        return new Queue(QUEUE_PROPOSTA_REJEITADA);
    }

    @Bean
    public Binding bindingCancel(Queue queueCancel, TopicExchange exchange) {
        return BindingBuilder.bind(queueCancel).to(exchange).with(ROUTING_KEY_PROPOSTA_REJEITADA);
    }

    @Bean
    public Queue queueUpdateStatus() {
        return new Queue(QUEUE_UPDATE_PROPOSTA_STATUS);
    }

    @Bean
    public Binding bindingUpdateStatus(Queue queueUpdateStatus, TopicExchange exchange) {
        return BindingBuilder.bind(queueUpdateStatus).to(exchange).with(ROUTING_KEY_UPDATE_PROPOSTA_STATUS);
    }

    @Bean
    public Queue queueUpdateStudent() {
        return new Queue(QUEUE_UPDATE_PROPOSTA_STUDENT);
    }

    @Bean
    public Binding bindingUpdate(Queue queueUpdateStudent, TopicExchange exchange) {
        return BindingBuilder.bind(queueUpdateStudent).to(exchange).with(ROUTING_KEY_UPDATE_PROPOSTA_STUDENT);
    }

    @Bean
    public Queue queueDelete() {
        return new Queue(QUEUE_DELETE);
    }

    @Bean
    public Binding bindingDelete(Queue queueDelete, TopicExchange exchange) {
        return BindingBuilder.bind(queueDelete).to(exchange).with(ROUTING_KEY_DELETE);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}