package com.example.prova_query.service;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.prova_query.enteties.ProvaEntity;
import com.example.prova_query.repo.ProvaRepo;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProvaService {

    @Autowired
    private ProvaRepo repository;


    public List<ProvaEntity> listAll() {
        return repository.findAll();
    }

    public void save(ProvaEntity prova) {
        repository.save(prova);
    }

    public void update(ProvaEntity prova) {
        ProvaEntity originalProva = repository.findByName(prova.getName());
    }

    public ProvaEntity getProva(String name) {

        return repository.findByName(name);
    }


    @Transactional
    public void delete(String name) {
        repository.deleteByName(name);
    }

}
