package com.example.sala_query.consumer;

import com.example.dto.ReservaSchema;
import com.example.dto.SalaSchema;
import com.example.sala_query.entities.Reserva;
import com.example.sala_query.entities.Sala;
import com.example.sala_query.salaConfig.Messaging;
import com.example.sala_query.service.ReservaService;
import com.example.sala_query.service.SalaService;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SalaService salaService;

    @Autowired
    private ReservaService reservaService;

    @RabbitListener(queues = Messaging.QUEUE_SALA)
    public void consumeMessageFromQueueSala(SalaSchema prova) {

        Sala provaConverted = modelMapper.map(prova, Sala.class);
        salaService.save(provaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_RESERVA)
    public void consumeMessageFromQueueReserva(ReservaSchema reserva) {

        Reserva reservaConverted = modelMapper.map(reserva, Reserva.class);
        reservaConverted.setSala(salaService.getSala(reserva.getSala()));



        reservaService.save(reservaConverted);
    }

}
