package com.example.sala_query.repo;

import com.example.sala_query.entities.Sala;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaRepo extends JpaRepository<Sala, Long> {

    Sala findByName(String name);
}
