package com.example.sala_query.entities;
import javax.persistence.*;

@Entity
public class Sala {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="salaId")
    private long salaId;
    @Column(nullable = false, unique = true)
    private String name;

    public long getSalaId() {
        return salaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
