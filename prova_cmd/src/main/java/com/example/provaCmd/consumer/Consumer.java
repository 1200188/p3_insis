package com.example.provaCmd.consumer;

import com.example.dto.ProvaSchema;
import com.example.provaCmd.provaConfig.Messaging;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private RabbitTemplate template;

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_SALA_CONFIRMADA)
    public void consumeMessageFromSalaConfirmada(ProvaSchema prova) {
        prova.setStatus("confirmed");
        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_PROVA_RECEBIDA, prova);
    }

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_SALA_REJEITADA)
    public void consumeMessageFromSalaRejeitada(ProvaSchema prova) {

        prova.setStatus("book rejected");
        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_PROVA_RECEBIDA, prova);
    }
}
