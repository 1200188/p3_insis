package com.example.provaCmd.routes;

import com.example.dto.*;
import com.example.provaCmd.provaConfig.Messaging;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RequestMapping("prova")
@RestController
public class Publisher {

    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public ResponseEntity<String> createProva(@RequestBody ProvaSchema prova) throws ParseException {


        //Validate date
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        try{
        Date dataProva = formatter.parse(prova.getDate());
        }
        catch(ParseException e) {
            return ResponseEntity.badRequest().body("{\"message\": \" Date format is invalid,  try the following format: yyyy-MM-dd'T'HH:mm:ss'Z' \"}");
        }


//        LocalDateTime limite = LocalDateTime.now().plusDays(7);
//        Date dataLimite = formatter.parse(limite.toString());
//
//        if (dataProva.compareTo(dataLimite))

        //Validate Name
        //and do I need this JSON media type for my use case?
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:9293/prova/provaName/" + prova.getName() , HttpMethod.GET, entity,String.class);

        if (response.getStatusCodeValue()==200) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Prova already exists \"}");
        }

        prova.setStatus("new");

        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_CREATE, prova);
        return ResponseEntity.accepted().headers(headers).body("{\"message\": \" The new 'Prova' is accepted \"}");
    }

    @PatchMapping("/bookProva/{name}/salaName/{salaName}")
    public ResponseEntity<String> roomProva(@PathVariable ("name") String nomeProva, @PathVariable ("salaName") String nomeSala) throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProva = restTemplate.exchange("http://localhost:9293/prova/provaName/" + nomeProva , HttpMethod.GET, entity,String.class);
        if (responseProva.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Prova do not exists \"}");
        }

            ObjectMapper mapper = new ObjectMapper();
            StringReader reader = new StringReader(responseProva.getBody());
            ProvaSchema prova = mapper.readValue(reader, ProvaSchema.class);

            if (!prova.getStatus().equals("new")&&!prova.getStatus().equals("book rejected")) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Booking in progress or already done \"}");
        }


//        ResponseEntity<String> responseSala = restTemplate.exchange("http://localhost:9293/prova/provaName/" + prova.getName() , HttpMethod.GET, entity,String.class);
//        if (responseProva.getStatusCodeValue()!=200) {
//            return ResponseEntity.badRequest().body("{\"message\": \" Sala do not exists \"}");
//        }

        prova.setStatus("Book in Progress");
        prova.setRoom(nomeSala);
        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_SALA_PROVA_RECEBIDA, prova);
        template.convertAndSend(Messaging.EXCHANGE_SALA, Messaging.ROUTING_KEY_SALA_RECEBIDA, prova);
        return ResponseEntity.accepted().body("{\"message\": \" The availability of Sala " + prova.getRoom() + "on date " + prova.getDate()+ " will be checked \"}");
    }


    @DeleteMapping("/deleteProva/{name}")
    public ResponseEntity<String> deleteProva(@PathVariable ("name") String nomeProva) throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseProva = restTemplate.exchange("http://localhost:9293/prova/provaName/" + nomeProva , HttpMethod.GET, entity,String.class);
        if (responseProva.getStatusCodeValue()!=200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Prova do not exists \"}");
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responseProva.getBody());
        ProvaSchema prova = mapper.readValue(reader, ProvaSchema.class);


        template.convertAndSend(Messaging.EXCHANGE_PROVA, Messaging.ROUTING_KEY_DELETE, prova);
        return ResponseEntity.accepted().headers(headers).body("{\"message\": \" The 'Prova' will be deleted \"}");
    }


}
