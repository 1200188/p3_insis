package com.example.pessoas_query.entities;

import javax.persistence.*;

@Entity
public class Pessoa {




    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int pessoaId;

    @Column(nullable = false, unique = true)
    private String name;
    private String role;
    private int rejected;
    private String LastModification;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }

    public String getLastModification() {
        return LastModification;
    }

    public void setLastModification(String lastModification) {
        LastModification = lastModification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPessoaId() {
        return pessoaId;
    }

    public void setPessoaId(int pessoaId) {
        this.pessoaId = pessoaId;
    }

}
