package com.example.pessoas_query.repo;

import com.example.pessoas_query.entities.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepo extends JpaRepository<Pessoa, Long> {

    Pessoa findByName(String name);

    Long deleteByName(String name);

    List<Pessoa> findByRoleAndStatus(String role, String status);

}
