package com.example.pessoas_query.consumer;


import com.example.dto.PessoaSchema;
import com.example.pessoas_query.config.Messaging;
import com.example.pessoas_query.entities.Pessoa;
import com.example.pessoas_query.service.PessoaService;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PessoaService pessoaService;


    @RabbitListener(queues = Messaging.QUEUE_CREATE)
    public void consumeMessageFromQueueCreate(PessoaSchema pessoa) {
        Pessoa pessoaConverted = modelMapper.map(pessoa, Pessoa.class);
        pessoaService.save(pessoaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_PESSOA)
    public void consumeMessageFromQueueUpdate(PessoaSchema pessoa) {
        Pessoa pessoaConverted = modelMapper.map(pessoa, Pessoa.class);
        pessoaService.save(pessoaConverted);

    }

    @RabbitListener(queues = Messaging.QUEUE_DELETE)
    public void consumeMessageFromQueueDelete(PessoaSchema pessoa) {
        pessoaService.delete(pessoa.getName());
    }

}
