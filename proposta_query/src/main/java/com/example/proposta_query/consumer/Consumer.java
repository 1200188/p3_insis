package com.example.proposta_query.consumer;

import com.example.dto.PropostaSchema;
import com.example.proposta_query.config.Messaging;
import com.example.proposta_query.entities.Proposta;
import com.example.proposta_query.service.PropostaService;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PropostaService pessoaService;


    @RabbitListener(queues = Messaging.QUEUE_CREATE)
    public void consumeMessageFromQueueCreate(PropostaSchema proposta) {
        Proposta propostaConverted = modelMapper.map(proposta, Proposta.class);
        pessoaService.save(propostaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_PROPOSTA_STATUS)
    public void consumeMessageFromQueueUpdateStatus(PropostaSchema proposta) {
        Proposta propostaConverted = modelMapper.map(proposta, Proposta.class);
        pessoaService.save(propostaConverted);

    }

    @RabbitListener(queues = Messaging.QUEUE_UPDATE_PROPOSTA_STUDENT)
    public void consumeMessageFromQueueUpdateStudent(PropostaSchema proposta) {
        Proposta propostaConverted = modelMapper.map(proposta, Proposta.class);
        pessoaService.save(propostaConverted);
    }

    @RabbitListener(queues = Messaging.QUEUE_DELETE)
    public void consumeMessageFromQueueDelete(PropostaSchema proposta) {
        pessoaService.delete(proposta.getName());
    }

}
