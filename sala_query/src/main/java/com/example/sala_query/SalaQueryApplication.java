package com.example.sala_query;

import com.example.sala_query.consumer.Consumer;
import com.example.sala_query.controller.Routes;
import com.example.sala_query.repo.ReservaRepo;
import com.example.sala_query.repo.SalaRepo;
import com.example.sala_query.salaConfig.Messaging;
import com.example.sala_query.service.ReservaService;
import com.example.sala_query.service.SalaService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Consumer.class, Messaging.class, SalaService.class, ReservaService.class, ReservaRepo.class, SalaRepo.class, Routes.class})
@EnableJpaRepositories("com.example.sala_query.repo")
public class SalaQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalaQueryApplication.class, args);
	}

}
