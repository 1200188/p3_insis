package com.example.provaCmd;

import com.example.provaCmd.consumer.Consumer;
import com.example.provaCmd.provaConfig.Messaging;
import com.example.provaCmd.routes.Publisher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Publisher.class, Messaging.class, Consumer.class})
public class ProvaCmdApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaCmdApplication.class, args);
	}

}
