package com.example.sala_query.salaConfig;

import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Messaging {

    ///Exchange Sala
    public static final String EXCHANGE_SALA = "sala";
    ///Command-->Query
    public static final String QUEUE_RESERVA = "reserva_sala";
    public static final String ROUTING_KEY_RESERVA = "reserva_sala_routingKey";
    public static final String QUEUE_SALA = "nova_sala";
    public static final String ROUTING_KEY_SALA = "nova_sala_routingKey";


    ///Beans 4 sala

    @Bean
    public TopicExchange exchangeSala() {
        return new TopicExchange(EXCHANGE_SALA);
    }

    @Bean
    public Queue queueReserva() {
        return new Queue(QUEUE_RESERVA);
    }

    @Bean
    public Binding bindingReserva(Queue queueReserva, TopicExchange exchangeSala) {
        return BindingBuilder.bind(queueReserva).to(exchangeSala).with(ROUTING_KEY_RESERVA);
    }

    @Bean
    public Queue queueSala() {
        return new Queue(QUEUE_SALA);
    }

    @Bean
    public Binding bindingSala(Queue queueSala, TopicExchange exchangeSala) {
        return BindingBuilder.bind(queueSala).to(exchangeSala).with(ROUTING_KEY_SALA);
    }

    //Rabbit Template a Object Converter

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
