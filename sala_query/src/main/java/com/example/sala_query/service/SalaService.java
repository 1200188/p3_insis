package com.example.sala_query.service;

import com.example.sala_query.entities.Reserva;
import com.example.sala_query.entities.Sala;
import com.example.sala_query.repo.SalaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class SalaService {

    @Autowired
    private SalaRepo repository;


    public List<Sala> listAll() {
        return repository.findAll();
    }

    public void save(Sala sala) {
        repository.save(sala);
    }

    public void update(Reserva reserva) {
        // Reserva originalReserva = repository.findByName(prova.getName());
    }

    public Sala getSala(String name) {

        return repository.findByName(name);
    }

    public Optional<Sala> getSalaById(long id) {

        return repository.findById(id);
    }


    public void delete(long id) {
        repository.deleteById(id);
    }

}
