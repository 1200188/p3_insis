package com.example.sala_cmd.controller;

import com.example.dto.ProvaSchema;
import com.example.dto.ReservaSchema;
import com.example.dto.SalaSchema;
import com.example.sala_cmd.salaConfig.Messaging;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

@RequestMapping("sala")
@RestController

public class Routes {

    @Autowired
    private RabbitTemplate template;

    @PostMapping
    public ResponseEntity<String> createSala(@RequestBody SalaSchema sala) throws ParseException {


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:9295/sala/salaName/" + sala.getName(), HttpMethod.GET, entity, String.class);

        if (response.getStatusCodeValue() == 200) {
            return ResponseEntity.badRequest().body("{\"message\": \" Sala already exists \"}");
        }

        template.convertAndSend(Messaging.EXCHANGE_SALA, Messaging.ROUTING_KEY_SALA, sala);
        return ResponseEntity.accepted().headers(headers).body("{\"message\": \" The new 'Sala' is accepted \"}");
    }

    @PostMapping("/evento")
    public ResponseEntity<String> createEvento(@RequestBody ReservaSchema reserva) throws ParseException, JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper mapper = new ObjectMapper();
        //Validate date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime limiteMax = LocalDateTime.now().plusDays(7);
        Date dataLimiteMax = formatter.parse(limiteMax.format(format));
        LocalDateTime limiteMin = LocalDateTime.now().plusDays(1);
        Date dataLimiteMin = formatter.parse(limiteMin.format(format));
        Date dataReserva = formatter.parse(reserva.getDate());

        if (!(dataReserva.compareTo(dataLimiteMax) < 0 && dataReserva.compareTo(dataLimiteMin) > 0)) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" The reservation is out of reservation period \"}");
        }

        //Validate Sala existence
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responseSala = restTemplate.exchange("http://localhost:9295/sala/salaName/" + reserva.getSala(), HttpMethod.GET, entity, String.class);
        if (responseSala.getStatusCodeValue() != 200) {
            return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Room do not exists \"}");
        }

        SalaSchema sala = mapper.readValue(responseSala.getBody(), SalaSchema.class);

        //Validate Sala availability
        ResponseEntity<String> responseReserva = restTemplate.exchange("http://localhost:9295/sala/reservas/", HttpMethod.GET, entity, String.class);
        System.out.println(responseReserva.getBody());
        ReservaSchema[] reservas = mapper.readValue(responseReserva.getBody(), ReservaSchema[].class);


        Calendar calendarNovaReserva = Calendar.getInstance();
        calendarNovaReserva.setTime(dataReserva);
        int newReservationStart = calendarNovaReserva.get(Calendar.HOUR_OF_DAY);
        int newReservationEnd = calendarNovaReserva.get(Calendar.HOUR_OF_DAY) + reserva.getDuration();

        for (ReservaSchema reservaDb : reservas) {

            Date dataReservaDB = formatter.parse(reservaDb.getDate());
            Calendar calendarReservaDB = Calendar.getInstance();
            calendarReservaDB.setTime(dataReservaDB);

            int existingReservationStart = calendarReservaDB.get(Calendar.HOUR_OF_DAY);
            int existingReservationEnd;

            if (reservaDb.getEvent()) {
                existingReservationEnd = calendarReservaDB.get(Calendar.HOUR_OF_DAY) + reservaDb.getDuration();
            } else {
                existingReservationEnd = calendarReservaDB.get(Calendar.HOUR_OF_DAY) + 2;
            }


            if (calendarReservaDB.get(Calendar.YEAR) == calendarNovaReserva.get(Calendar.YEAR) && calendarReservaDB.get(Calendar.DAY_OF_YEAR) == calendarNovaReserva.get(Calendar.DAY_OF_YEAR)) {
                if (newReservationStart >= existingReservationStart && newReservationStart <= existingReservationEnd) {
                    return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Book is overlapped by another book \"}");
                } else if (existingReservationStart >= newReservationStart && existingReservationStart <= newReservationEnd) {
                    return ResponseEntity.badRequest().headers(headers).body("{\"message\": \" Book is overlapped by another book \"}");
                }
            }


        }

        template.convertAndSend(Messaging.EXCHANGE_SALA, Messaging.ROUTING_KEY_RESERVA, reserva);
        return ResponseEntity.accepted().body("{\"message\": \" The new 'Reserva' is accepted \"}");
    }



}
