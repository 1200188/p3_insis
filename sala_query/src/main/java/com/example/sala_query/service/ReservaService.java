package com.example.sala_query.service;

import com.example.sala_query.entities.Reserva;
import com.example.sala_query.repo.ReservaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservaService {

    @Autowired
    private ReservaRepo repository;




    public List<Reserva> listAll() {
        return repository.findAll();
    }

    public void save(Reserva reserva) {
        repository.save(reserva);
    }

    public void update(Reserva reserva) {
    }

    public Reserva getReserva(String provaName) {

        return repository.findByProva(provaName);
    }





}
