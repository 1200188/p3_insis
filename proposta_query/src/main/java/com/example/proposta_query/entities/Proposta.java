package com.example.proposta_query.entities;

import javax.persistence.*;

@Entity
public class Proposta {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int propostaId;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String proponent;

    private String lastModification;
    private String status;
    private String student;


    public int getPropostaId() {
        return propostaId;
    }

    public void setPropostaId(int propostaId) {
        this.propostaId = propostaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastModification() {
        return lastModification;
    }

    public void setLastModification(String lastModification) {
        this.lastModification = lastModification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProponent() {
        return proponent;
    }

    public void setProponent(String proponent) {
        this.proponent = proponent;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
