package com.example.proposta_query;

import com.example.proposta_query.config.Messaging;
import com.example.proposta_query.consumer.Consumer;
import com.example.proposta_query.controller.Routes;
import com.example.proposta_query.repo.PropostaRepo;
import com.example.proposta_query.schedules.Schedule;
import com.example.proposta_query.service.PropostaService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Schedule.class, Consumer.class, Messaging.class, PropostaService.class, PropostaRepo.class, Routes.class})
@EnableJpaRepositories("com.example.proposta_query.repo")
@EnableScheduling
public class PropostaQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropostaQueryApplication.class, args);
	}

}
