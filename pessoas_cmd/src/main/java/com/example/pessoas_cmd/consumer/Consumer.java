package com.example.pessoas_cmd.consumer;

import com.example.dto.PessoaSchema;
import com.example.dto.PropostaSchema;
import com.example.pessoas_cmd.config.Messaging;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class Consumer {


    @Autowired
    private RabbitTemplate template;

    @RabbitListener(queues = Messaging.QUEUE_PROPOSTA_REJEITADA)
    public void consumeMessageFromQueueCreate(PropostaSchema proposta) throws IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set my entity
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<String> responsePessoa = restTemplate.exchange("http://localhost:9297/pessoa/pessoaName/" + proposta.getProponent() , HttpMethod.GET, entity,String.class);

        if (responsePessoa.getStatusCodeValue()!=200) {
            return;
        }

        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(responsePessoa.getBody());
        PessoaSchema pessoa = mapper.readValue(reader, PessoaSchema.class);

        int countToInactive=pessoa.getRejected()+1;

        if (countToInactive==3){
        pessoa.setStatus("inactive");
        }

        pessoa.setRejected(countToInactive);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime now = LocalDateTime.now();
        pessoa.setLastModification(now.format(format).toString());


        template.convertAndSend(Messaging.EXCHANGE_PESSOA, Messaging.ROUTING_KEY_UPDATE_PESSOA, pessoa);

    }


}
