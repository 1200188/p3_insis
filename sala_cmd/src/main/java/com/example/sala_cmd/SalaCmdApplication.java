package com.example.sala_cmd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalaCmdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalaCmdApplication.class, args);
	}

}
