package com.example.pessoas_query.schedules;

import com.example.pessoas_query.entities.Pessoa;
import com.example.pessoas_query.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class Schedule {

    @Autowired
    private PessoaService pessoaService;

    @Scheduled(cron = "0 0 0 30 7 ?")
    public void setStudentStatus() throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime now = LocalDateTime.now();
        Date today = formatter.parse(now.format(format));

        Calendar calendarToday = Calendar.getInstance();
        calendarToday.setTime(today);


       if(calendarToday.get(Calendar.MONTH)==7){

        List<Pessoa> updateList=pessoaService.listByRoleAndStatus("student","active");

        for(Pessoa pessoa:updateList){
            pessoa.setStatus("inactive");
            pessoa.setLastModification(today.toString());
            pessoaService.save(pessoa);
        }
       }

    }

    @Scheduled(cron = "0 0 0 * * *")
    public void setTeacherStatus() throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime now = LocalDateTime.now();
        Date today = formatter.parse(now.format(format));

        LocalDateTime expirationTime = LocalDateTime.now().minusMonths(4);
        Date expirationDate = formatter.parse(expirationTime.format(format));


        List<Pessoa> updateList=pessoaService.listByRoleAndStatus("teacher","inactive");

        for(Pessoa pessoa:updateList){

            Date lasUpdateDate = formatter.parse(pessoa.getLastModification());
            if(lasUpdateDate.compareTo(expirationDate)<0){
                pessoa.setStatus("active");
                pessoa.setLastModification(today.toString());
                pessoaService.save(pessoa);
            }

        }
    }
}
