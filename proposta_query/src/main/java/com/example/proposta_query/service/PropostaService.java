package com.example.proposta_query.service;

import com.example.proposta_query.entities.Proposta;
import com.example.proposta_query.repo.PropostaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PropostaService {

    @Autowired
    private PropostaRepo repository;


    public List<Proposta> listAll() {
        return repository.findAll();
    }

    public List<Proposta> listByStatus(String status) {
        return repository.findByStatus(status);
    }

    public void save(Proposta proposta) {
        repository.save(proposta);
    }

    public Proposta getProposta(String name) {

        return repository.findByName(name);
    }


    @Transactional
    public void delete(String name) {
        repository.deleteByName(name);
    }
}
