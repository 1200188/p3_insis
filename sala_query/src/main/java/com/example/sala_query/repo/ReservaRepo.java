package com.example.sala_query.repo;

import com.example.sala_query.entities.Reserva;
import com.example.sala_query.entities.Sala;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservaRepo extends JpaRepository<Reserva, Long> {
    Reserva findByProva(String provaName);
}
