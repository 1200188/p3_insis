package com.example.pessoas_query;

import com.example.pessoas_query.config.Messaging;
import com.example.pessoas_query.consumer.Consumer;
import com.example.pessoas_query.controller.routes;
import com.example.pessoas_query.repo.PessoaRepo;
import com.example.pessoas_query.schedules.Schedule;
import com.example.pessoas_query.service.PessoaService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Schedule.class, Consumer.class, Messaging.class, PessoaService.class, PessoaRepo.class, routes.class})
@EnableJpaRepositories("com.example.pessoas_query.repo")
@EnableScheduling
public class PessoasQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(PessoasQueryApplication.class, args);
	}

}
