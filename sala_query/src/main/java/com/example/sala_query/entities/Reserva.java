package com.example.sala_query.entities;
import javax.persistence.*;

@Entity
public class Reserva {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int reservaId;
    @ManyToOne(cascade=CascadeType.MERGE)
    @JoinColumn(name = "salaId", referencedColumnName = "salaId")
    private Sala sala;
    private String date;
    private String prova;
    private boolean event;
    private int duration;

    public int getReservaId() {
        return reservaId;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProvaName() {
        return prova;
    }

    public void setProvaName(String provaName) {
        this.prova = prova;
    }

    public boolean getEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
