package com.example.sala_query.controller;

import com.example.dto.ReservaSchema;
import com.example.sala_query.entities.Reserva;
import com.example.sala_query.service.ReservaService;
import com.example.sala_query.service.SalaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("sala")
@Controller
public class Routes {

    @Autowired
    private SalaService salaService;

    @Autowired
    private ReservaService reservaService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/salaName/{name}")
    public ResponseEntity<String> getSala(@PathVariable("name") String nomeSala) throws JsonProcessingException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper Obj = new ObjectMapper();
        String body = Obj.writeValueAsString(salaService.getSala(nomeSala));

        if (body.equals("null"))
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }

    @GetMapping("/salaId/{id}")
    public ResponseEntity<String> getSalaById(@PathVariable("id") long id) throws JsonProcessingException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper Obj = new ObjectMapper();
        String body = Obj.writeValueAsString(salaService.getSalaById(id));

        if (body.equals("null"))
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }

    @GetMapping("/reservas/prova/{name}")
    public ResponseEntity<String> getReserva(@PathVariable ("name") String nomeProva) throws JsonProcessingException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper Obj = new ObjectMapper();
        String body = Obj.writeValueAsString(reservaService.getReserva(nomeProva));

        if (body.equals("null"))
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }

    @GetMapping("/reservas")
    public ResponseEntity<String> getReservas() throws JsonProcessingException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper Obj = new ObjectMapper();

        List <Reserva> reservas = reservaService.listAll();
        ArrayList<ReservaSchema> reservasBodyList = new ArrayList<ReservaSchema>();

        for(Reserva reservaMapa:reservas){

            String salaName=reservaMapa.getSala().getName();
            reservaMapa.setSala(null);

            ReservaSchema reservaBody = modelMapper.map(reservaMapa, ReservaSchema.class);

            reservaBody.setSala(salaName);

            reservasBodyList.add(reservaBody);

        }

        String body= Obj.writeValueAsString(reservasBodyList);

        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }
}
