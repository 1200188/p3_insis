package com.example.proposta_cmd;

import com.example.proposta_cmd.config.Messaging;
import com.example.proposta_cmd.controller.Routes;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Messaging.class,  Routes.class})
public class PropostaCmdApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropostaCmdApplication.class, args);
	}

}
