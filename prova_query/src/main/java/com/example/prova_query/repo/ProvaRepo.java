package com.example.prova_query.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.prova_query.enteties.ProvaEntity;



@Repository
public interface ProvaRepo extends JpaRepository<ProvaEntity, Long> {

    ProvaEntity findByName(String name);

    Long deleteByName(String name);
}
