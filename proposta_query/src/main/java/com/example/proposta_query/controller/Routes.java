package com.example.proposta_query.controller;

import com.example.proposta_query.service.PropostaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@RequestMapping("proposta")
@Controller
public class Routes {

    @Autowired
    private PropostaService service;

    @GetMapping("/propostas")
    public ResponseEntity<String> viewHomePage(Model model) throws JsonProcessingException {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper Obj = new ObjectMapper();
        String body = Obj.writeValueAsString(service.listAll());
        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }

    @GetMapping("/propostaName/{name}")
    public ResponseEntity<String> getPessoa(@PathVariable("name") String nomeProposta) throws JsonProcessingException {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper Obj = new ObjectMapper();
        String body = Obj.writeValueAsString(service.getProposta(nomeProposta));

        if (body.equals("null"))
            return ResponseEntity.noContent().build();

        return ResponseEntity.ok().headers(responseHeaders).body(body);
    }
}
