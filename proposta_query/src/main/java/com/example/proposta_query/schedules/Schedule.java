package com.example.proposta_query.schedules;


import com.example.proposta_query.config.Messaging;
import com.example.proposta_query.entities.Proposta;
import com.example.proposta_query.service.PropostaService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Component
public class Schedule {

    @Autowired
    private PropostaService propostaService;

    @Autowired
    private RabbitTemplate template;

    @Scheduled(cron = "0 0 0 * * *")
    public void cancelUnaccepted() throws ParseException {

        List<Proposta> updateList=propostaService.listByStatus("new");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime now = LocalDateTime.now();
        Date today = formatter.parse(now.format(format));

        LocalDateTime expirationTime = LocalDateTime.now().minusMonths(3);
        Date expirationDate = formatter.parse(expirationTime.format(format));


        for(Proposta proposta:updateList){

            Date lasUpdateDate = formatter.parse(proposta.getLastModification());
            if(lasUpdateDate.compareTo(expirationDate)<0){
                proposta.setStatus("canceled");
                proposta.setLastModification(today.toString());
                propostaService.save(proposta);
                template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_PROPOSTA_REJEITADA, proposta);
            }

        }
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void cancelNoStudent() throws ParseException {

        List<Proposta> updateList=propostaService.listByStatus("accepted");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        LocalDateTime now = LocalDateTime.now();
        Date today = formatter.parse(now.format(format));

        LocalDateTime expirationTime = LocalDateTime.now().minusMonths(6);
        Date expirationDate = formatter.parse(expirationTime.format(format));


        for(Proposta proposta:updateList){

            Date lasUpdateDate = formatter.parse(proposta.getLastModification());
            if(proposta.getStudent()==null&&lasUpdateDate.compareTo(expirationDate)<0){
                proposta.setStatus("canceled");
                proposta.setLastModification(today.toString());
                propostaService.save(proposta);
                template.convertAndSend(Messaging.EXCHANGE_PROPOSTA, Messaging.ROUTING_KEY_PROPOSTA_REJEITADA, proposta);
            }

        }

    }
}
