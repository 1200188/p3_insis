package com.example.proposta_query.repo;

import com.example.proposta_query.entities.Proposta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PropostaRepo extends JpaRepository<Proposta, Long> {

    Proposta findByName(String name);

    Long deleteByName(String name);

    List<Proposta> findByStatus(String status);

}
