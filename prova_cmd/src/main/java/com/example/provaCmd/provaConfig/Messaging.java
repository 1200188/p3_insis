package com.example.provaCmd.provaConfig;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class Messaging {

    ///Exchange Prova
    public static final String EXCHANGE_PROVA = "prova";
    public static final String QUEUE_CREATE = "prova_recebida";
    public static final String QUEUE_UPDATE_SALA_PROVA_RECEBIDA = "sala_recebida";
    public static final String QUEUE_UPDATE_PROVA_CONFIRMADA = "prova_confirmada";
    public static final String QUEUE_DELETE = "delete_prova";
    public static final String QUEUE_UPDATE_SALA_REJEITADA = "sala_rejeitada";
    public static final String QUEUE_UPDATE_SALA_CONFIRMADA = "sala_confirmada";

    public static final String ROUTING_KEY_CREATE = "prova_recebida_routingKey";
    public static final String ROUTING_KEY_SALA_PROVA_RECEBIDA = "sala_recebida_routingKey";
    public static final String ROUTING_KEY_PROVA_CONFIRMADA = "prova_confirmada_routingKey";
    public static final String ROUTING_KEY_DELETE = "delete_prova_routingKey";
    public static final String ROUTING_KEY_SALA_CONFIRMADA = "sala_confirmada_routingKey";
    public static final String ROUTING_KEY_SALA_REJEITADA = "sala_rejeitada_routingKey";

    ///Exchange Sala
    public static final String EXCHANGE_SALA = "sala";
    public static final String QUEUE_UPDATE_SALA_RECEBIDA = "sala_prova_recebida";
    public static final String ROUTING_KEY_SALA_RECEBIDA = "sala_prova_recebida_routingKey";


    ///Beans 4 sala
    @Bean
    public TopicExchange exchangeSala() {
        return new TopicExchange(EXCHANGE_SALA);
    }
    @Bean
    public Queue queueCreateSalaProva() {
        return new Queue(QUEUE_UPDATE_SALA_RECEBIDA);
    }

    @Bean
    public Binding bindingSalaProva(Queue queueCreateSalaProva, TopicExchange exchangeSala) {
        return BindingBuilder.bind(queueCreateSalaProva).to(exchangeSala).with(ROUTING_KEY_SALA_RECEBIDA);
    }


    ///Beans 4 prova
    @Bean
    public TopicExchange exchangeProva() {
        return new TopicExchange(EXCHANGE_PROVA);
    }

    @Bean
    public Queue queueCreate() {
        return new Queue(QUEUE_CREATE);
    }

    @Bean
    public Binding bindingCreate(Queue queueCreate, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueCreate).to(exchangeProva).with(ROUTING_KEY_CREATE);
    }

    @Bean
    public Queue queueSalaProvaRecebida() {
        return new Queue(QUEUE_UPDATE_SALA_PROVA_RECEBIDA);
    }

    @Bean
    public Binding bindingSalaProvaRecebida(Queue queueSalaProvaRecebida, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueSalaProvaRecebida).to(exchangeProva).with(ROUTING_KEY_SALA_PROVA_RECEBIDA);
    }

    @Bean
    public Queue queueSalaProvaConfirmada() {
        return new Queue(QUEUE_UPDATE_PROVA_CONFIRMADA );
    }

    @Bean
    public Binding bindingProvaConfirmada(Queue queueSalaProvaConfirmada, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueSalaProvaConfirmada).to(exchangeProva).with(ROUTING_KEY_PROVA_CONFIRMADA);
    }

    @Bean
    public Queue queueDelete() {
        return new Queue(QUEUE_DELETE);
    }

    @Bean
    public Binding bindingDelete(Queue queueDelete, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueDelete).to(exchangeProva).with(ROUTING_KEY_DELETE);
    }

    @Bean
    public Queue queueSalaConfirmada() {
        return new Queue(QUEUE_UPDATE_SALA_CONFIRMADA);
    }

    @Bean
    public Binding bindingSalaConfirmada(Queue queueSalaConfirmada, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueSalaConfirmada).to(exchangeProva).with(ROUTING_KEY_SALA_CONFIRMADA);
    }

    @Bean
    public Queue queueSalaRejeitada() {
        return new Queue(QUEUE_UPDATE_SALA_REJEITADA);
    }

    @Bean
    public Binding bindingSalaRejeitada(Queue queueSalaRejeitada, TopicExchange exchangeProva) {
        return BindingBuilder.bind(queueSalaRejeitada).to(exchangeProva).with(ROUTING_KEY_SALA_REJEITADA);
    }



    //Rabbit Template a Object Converter

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}
