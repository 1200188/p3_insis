package com.example.pessoas_query.config;

import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class Messaging {

    ///Exchange Pessoa
    public static final String EXCHANGE_PESSOA = "pessoa";
    public static final String QUEUE_CREATE = "pessoa_recebida";
    public static final String QUEUE_UPDATE_PESSOA = "role_pessoa_recebida";
    public static final String QUEUE_DELETE = "delete_pessoa";
    public static final String ROUTING_KEY_CREATE = "pessoa_recebida_routingKey";
    public static final String ROUTING_KEY_UPDATE_PESSOA = "role_pessoa_recebida_routingKey";
    public static final String ROUTING_KEY_DELETE = "delete_pessoa_routingKey";




    ///Beans 4 Pessoa
    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_PESSOA);
    }

    @Bean
    public Queue queueCreate() {
        return new Queue(QUEUE_CREATE);
    }

    @Bean
    public Binding bindingCreate(Queue queueCreate, TopicExchange exchange) {
        return BindingBuilder.bind(queueCreate).to(exchange).with(ROUTING_KEY_CREATE);
    }

    @Bean
    public Queue queueUpdate() {
        return new Queue(QUEUE_UPDATE_PESSOA);
    }

    @Bean
    public Binding bindingUpdate(Queue queueUpdate, TopicExchange exchange) {
        return BindingBuilder.bind(queueUpdate).to(exchange).with(ROUTING_KEY_UPDATE_PESSOA);
    }

    @Bean
    public Queue queueDelete() {
        return new Queue(QUEUE_DELETE);
    }

    @Bean
    public Binding bindingDelete(Queue queueDelete, TopicExchange exchange) {
        return BindingBuilder.bind(queueDelete).to(exchange).with(ROUTING_KEY_DELETE);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }

}
