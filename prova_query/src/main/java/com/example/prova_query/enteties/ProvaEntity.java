package com.example.prova_query.enteties;

import javax.persistence.*;

@Entity
public class ProvaEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int provaId;
    @Column(nullable = false, unique = true)
    private String name;
    private String date;
    private String room;
    private String status;

    public ProvaEntity() {

    }
    public ProvaEntity(String name, String date, String room, String status) {


        this.name = name;
        this.room = room;
        this.date = date;
        this.status= "new";
    }


    public int getProvaId() {
        return provaId;
    }

    public void setProvaId(int provaId) {
        this.provaId = provaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
