package com.example.pessoas_query.service;

import com.example.pessoas_query.entities.Pessoa;
import com.example.pessoas_query.repo.PessoaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepo repository;


    public List<Pessoa> listAll() {
        return repository.findAll();
    }

    public List<Pessoa> listByRoleAndStatus(String role, String status) {
        return repository.findByRoleAndStatus(role, status);
    }

    public void save(Pessoa pessoa) {
        repository.save(pessoa);
    }

    public void update(Pessoa pessoa) {
        Pessoa originalPessoa = repository.findByName(pessoa.getName());
    }

    public Pessoa getPessoa(String name) {

        return repository.findByName(name);
    }


    @Transactional
    public void delete(String name) {
        repository.deleteByName(name);
    }

}


