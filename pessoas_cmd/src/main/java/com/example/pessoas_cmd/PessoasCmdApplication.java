package com.example.pessoas_cmd;

import com.example.pessoas_cmd.config.Messaging;
import com.example.pessoas_cmd.consumer.Consumer;
import com.example.pessoas_cmd.controller.routes;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {Messaging.class,  routes.class, Consumer.class})
public class PessoasCmdApplication {

	public static void main(String[] args) {
		SpringApplication.run(PessoasCmdApplication.class, args);
	}

}
